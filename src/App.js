import './App.css';
import Navbar from './component/Navbar';
import Home from './component/Home';
import { Switch, Route } from 'react-router-dom';
import Products from './component/Products';
import Product from './component/Product';
import Footer from './component/Footer';
import About from './component/About';
import Contact from './component/Contact';
import Cart from './component/Cart';
import Checkout from './component/Checkout';
import Login from './component/Login';
import Signup from './component/Signup';
import Logout from './component/Logout';
import React from 'react';
import Dashboard from './component/Dashboard';
import Inventory from './component/pages/Inventory';
import Orders from './component/pages/Orders';
import Addproduct from './component/pages/Addproduct';

function App() {



  return (
    <>
    <Navbar/>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/products" component={Products} />
        <Route exact path="/products/:id" component={Product} />
        <Route exact path="/cart" component={Cart} />
         <Route exact path="/checkout" component={Checkout} />
         <Route exact path="/about" component={About} />
         <Route exact path="/contact" component={Contact} />
         <Route exact path="/login" component={Login} />
         <Route exact path="/signup" component={Signup} />
         <Route exact path="/logout" component={Logout} />
         <Route exact path="/dashboard" component={Dashboard} />
         <Route exact path="/allProducts" component={Inventory} />
         <Route exact path="/orders" component={Orders} />
         <Route exact path="/addProduct" component={Addproduct}/>
      </Switch>
      <Footer/>
    </>
  );
}

export default App;
